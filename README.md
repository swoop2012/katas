Preparation to Symfony certification

See different branches for different tasks.

kata-3 - Created a console command

kata-4 - Created a compiler pass to inject a service to another service by tag

kata-5 - Customized a data collector , added item in menu and a page in profiler

kata-6 -  Added MofelTransformer and ViewTransformer to handle Many-To-Many relation. 

kata-7 - Created FormType to hold the repetitive data for a couple of other form types

kata-8 - Wokrking with ElasticSearch.  Added repository for search. Added transformer for an Entity

kata-9 - Added event listener and changed it to event subscriber to perform the check in the app

 kata-10 - Worked with Varnish and ESI. Part of the content is cached and other part is dynamic
